<!--
SPDX-FileCopyrightText: 2023 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

Build YP Poky core-image-base for QEMU x86-64 and use YP's sstate cache server
```
kas build kas-conf/distro-poky.yaml:kas-conf/machine-qemux86-64.yaml:kas-conf/tweak-yp-sstate.yaml
```

Run YP Poky core-image-base on QEMU x86-64.
```
kas shell -c "runqemu core-image-base kvm serial slirp audio nographic" kas-conf/distro-poky.yaml:kas-conf/machine-qemux86-64.yaml
```
